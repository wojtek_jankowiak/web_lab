pi = parseFloat("3.14");
document.writeln(
    "<h1>Welcome to simple JavaScript game, " + getUserName() +", Pi is equal to " +pi+ "!</h1>" );

var score = 0;
i = parseInt(100000, 2);


function play(){
    setTime();
    generateButton();
    i = parseInt(100000, 2);
    window.addEventListener("scroll", varn);
    var h = document.getElementById("h2.2");
    var t = document.createTextNode("Hello World");
    h.appendChild(t); 
}

function varn(){
    window.alert("There is nothing down there!")
}

function getUserName(){
    return window.prompt("Podaj swoje imię", "Stranger");
}

function setTime(){
    document.getElementById('h2.1').innerHTML = "Time left: " + i;
    i--;
    if (i < 0) {
        //alert('You lose!');
        var txt = "LOSER";
        for (x of txt) {
            document.write(x + "<br >");
          }
    }
    else {
        setTimeout(setTime, 1000);
    }
}

function destroyButton(){
    var element = document.getElementById("my_btn");
    element.parentNode.removeChild(element);
}

function youAreDoingGreat(){
    switch(score) {
        case 10:
            window.alert("You are doing great!")
          break;
        case 100:
            window.alert("You are the best!")
          break;
      }
}

function endGame(){
    destroyButton();
    score = 0;
    document.getElementById("h2").innerHTML = "Your score is: " + score;
    i = 0;
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function addScore(){
    score = score + 1;
}

function generateButton(){
    document.getElementById("h2").innerHTML = "Your score is: " + score;
    document.getElementById("my_div").style.left = randomInt(0, 1300) + "px";
    document.getElementById("my_div").style.top = randomInt(0, 600) + "px";
    var btn = document.createElement("BUTTON");
    btn.addEventListener("click", addScore);
    youAreDoingGreat();
    btn.addEventListener("click", destroyButton);
    btn.addEventListener("click", generateButton);
    btn.innerHTML = "CLICK ME!";
    btn.style.width = "100px";
    btn.style.height = "100px";
    btn.style.fontSize = "20px";
    btn.setAttribute("id", "my_btn");
    document.getElementById("my_div").appendChild(btn);
}