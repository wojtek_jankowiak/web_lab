const myText = document.getElementById('h3.1');

myText.addEventListener('mousedown', setTextDown);

function setTextDown(){
    document.getElementById('h3.1').innerHTML = "DOWN";
}

i = 0;
while(i > 10){
    document.writeln(
        "<h1>1 is greater than 10</h1>" );
}
document.writeln(
    "<h1>1 is smaller than 10</h1>" );


do{
    document.writeln(
        "<h1>1 is greater than 10</h1>" );
}while(i > 10)


function isKeyPressed(event) {
    if (event.altKey) {
      alert("The ALT key was pressed!");
    }
    else if (event.ctrlKey){
        alert("The CTRL key was pressed!");
    }
    else if (event.shiftKey){
        alert("The SHIFT key was pressed!");
    }
    else if (event.keyCode == 13)
    {
        alert("The J key was pressed!");
    }
  }

function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var x1 = event.screenX;
    var y1 = event.screenY;
    var coords = "X coords: " + x + ", Y coords: " + y + ", X screen coords: " + x1 + ", Y screen coords: " +y1;
    document.getElementById("h1.1").innerHTML = coords;
  }


function my_fun(){
    var newItem = document.createElement("LI");       
    var textnode = document.createTextNode("Trzy");  
    newItem.appendChild(textnode);                    

    var list = document.getElementById("mojaLista");   
    list.insertBefore(newItem, list.childNodes[0]);  
}

function redBackground(){
    document.body.style.backgroundColor = "red";
    document.getElementById("h1").style.color = "aquamarine";
    document.getElementById("h1.1").style.color = "aquamarine";
}

function aquamarineBackground(){
    document.body.style.backgroundColor = "aquamarine";
    document.getElementById("h1").style.color = "red";
    document.getElementById("h1.1").style.color = "red";
}

function replace(){
    document.getElementById("h3").innerHTML = "Liczba linków: " + document.links.length +", liczba formularzy: " + document.forms.length + ", liczba kotwic: " + document.anchors.length;
    var x = document.images.item(0).src;
    var textnode = document.createTextNode(x);

    var item = document.getElementById("mojaLista").childNodes[0];
    
    item.replaceChild(textnode, item.childNodes[0]);
}

function alertNamedItem() {
    var x = document.getElementsByTagName("P").namedItem("mojeP");
    alert(x.innerHTML);
  }

function changeItalic(){
    document.getElementById("h1").style.fontStyle = "italic";
}

function changeSansSerif(){
    document.getElementById("h1").style.fontFamily = "sans-serif";
}

function changeImpact(){
    document.getElementById("h1").style.fontFamily = "Impact";
}

function alertMouseDown(){
    alert("Mouse down!");
}

function alertMouseUp(){
    alert("Mouse up!")
}
