﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Imie: 
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Wymagane!" ForeColor="Red"></asp:RequiredFieldValidator>
            </p>
            <p>Email: 
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox2" ErrorMessage="Błędny adres email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
            </p>
            <p>Powtórz email: <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox2" ControlToValidate="TextBox4" ErrorMessage="Adresy email nie są takie same!" ForeColor="Red"></asp:CompareValidator>
            </p>
            <p>Numer telefonu: 
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox3" ErrorMessage="Błędny numer telefonu!" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ForeColor="Red"></asp:RegularExpressionValidator>
            </p>
            <p>Wiek: 
                <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBox5" ErrorMessage="Błędny zakres!" ForeColor="Red" MaximumValue="99" MinimumValue="1"></asp:RangeValidator>
            </p>
        </div>
        <p>
            <asp:Label ID="Label2" runat="server"></asp:Label>
        </p>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wyślij dane" />
        <asp:Button ID="Button2" runat="server" CausesValidation="False" OnClick="Button2_Click" Text="Pokaż label" />
    <p>
        &nbsp;</p>
        <asp:Label ID="Label3" runat="server" Text="Ten label był niewidoczny"></asp:Label>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Imię"></asp:Label>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Nazwisko"></asp:Label>
        <br />
        <asp:Label ID="Label6" runat="server" Text="Email"></asp:Label>
        <br />
        <asp:Label ID="Label7" runat="server" Text="Telefon"></asp:Label>
        <br />
        <asp:Label ID="Label8" runat="server" Text="Wiek"></asp:Label>
    </form>
    </body>
</html>
